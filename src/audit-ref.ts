export interface AuditRef {
    id: string;
    weight: number;
    group: string;
}

export interface Audit {
    id: string;
    title: string;
    description: string;
    score: number;
    scoreDisplayMode: string;
    numericValue: number;
    numericUnit: string;
    displayValue: string;
    details: any;
    warnings: any;
    type: string;
    overallSavingsMs: number;
}