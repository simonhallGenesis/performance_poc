import {v4 as uuidv4} from 'uuid';
import {getLighthouseReport} from './tools';
import * as data from './bigquery';

export async function lh_runner(url: string, write?: boolean)  { 
 
  const bq = write?write:true;
  const local = false;
  const request_id: string = uuidv4();
  console.log("Processing request " + request_id + " for site " + url);
  const report: any = await getLighthouseReport(url);

  const fetchTime: string = report.fetchTime;
  
  data.saveRaw(report,request_id,local,bq);
  data.saveCategories(report,request_id,local,bq);
  data.saveCategoryBreakdown(report,request_id,local,bq);    
  data.saveGeneral(report,request_id,local,bq);
  data.saveAudits(report,request_id,local,bq);
  data.saveTiming(report,request_id,fetchTime,local,bq);
  data.saveAuditExtras(report,request_id,fetchTime,local,bq);

  console.log(request_id + " completed");
  
}
