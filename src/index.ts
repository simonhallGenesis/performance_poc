import {lh_runner} from './lh_runner';
import type {HttpFunction, EventFunction} from '@google-cloud/functions-framework/build/src/functions';

export const lh_auto_web: HttpFunction = async (req, res) => { 

    const url: string = req.query.url as string;
    if (url && url != undefined){ 
        
        await lh_runner(url,false);
        res.status(200).send("success!");

    } else {
      console.log("Could not find parameter for url containing website to test")
      res
        .status(400)
        .send('Could not find parameter for url containing website to test');
    }

}

export const lh_auto: EventFunction = ( async (event: any, context: any) => {

    if (event.data) {
        const url: string = Buffer.from(event.data, 'base64').toString();
    
        if (url && url != undefined){ 
            await lh_runner(url);
        }  else {
            console.log("url not found in message body, function not executed");
        }
    } else {
        console.log("Event message is empty, function not executed");
    }

});
