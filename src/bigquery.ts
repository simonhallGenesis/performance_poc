import {Category} from './category';
import {Audit} from './audit-ref';
import {addMs, insertData, round} from './tools';
import { Timing } from './timing';
import { AuditExtra } from './audit-extra';

export function getData(input: any[], path: string): any[] {
    let out: any[] = [];
    input.forEach(inp => out.push(inp[path]));
    return out;
}

export async function saveRaw(report: any, request_id: string, local: boolean, bq: boolean) {
    const raw = [
        {
            request_id: request_id,
            data: bq ? JSON.stringify(report) : JSON.stringify(report).substr(0,100)
        }
      ];
  
      await insertData(raw,'performance_poc','raw',local,bq);
}

export async function saveCategories(report: any, request_id: string, local: boolean, bq: boolean){
    const categories: Category[] = Object.values(report.categories).map(element => {
        return element as Category;
      });
  
      let cats: any[] = [];
      categories.forEach(cat => {
        cats.push(
          {
            request_id:request_id,
            category:cat.id,
            score:cat.score
          }
        );
      });
  
      await insertData(cats,'performance_poc','categories',local,bq);
}

export async function saveAuditExtras(report: any, request_id: string, fetchTime: string, local: boolean, bq: boolean) {
    const toKeep: string[] = [
        'user-timings','mainthread-work-breakdown','bootup-time','network-requests'
        ,'network-rtt','network-server-latency','resource-summary','total-byte-weight'
        ,'long-tasks','render-blocking-resources','unused-css-rules','unused-javascript','uses-optimized-images'
    ];

    const audits: Audit[] = Object.values(report.audits)
    .map(element => {
        return element as Audit;
    }).filter(audit => {
        return toKeep.includes(audit.id);
    });
    const rows: any[] = [];

    let newtime: string = addMs(report.fetchTime, 3100);
    
    audits.forEach(audit => {
        let rowNum: number = 0;
        if (audit.details && audit.details.items) {
            audit.details.items.forEach((item: any) => {
                rowNum++;
                Object.keys(item).forEach((k:string) =>
                {
                    if (item[k] && item[k]!= undefined){
                        rows.push({
                            request_id: request_id,
                            audit_id: audit.id,
                            column: k,
                            row: rowNum,
                            value_str: isNaN(+item[k])?item[k]:undefined,
                            value_num: isNaN(+item[k])?undefined:round(item[k])
                        });
                    }
                });
            });
        }
    });

    await insertData(rows,'performance_poc','audit_extras',local,bq);
}

export async function saveCategoryBreakdown(report: any, request_id: string, local: boolean, bq: boolean){
    const categories: Category[] = Object.values(report.categories).map(element => {
        return element as Category;
      });

    let refs: any[] = [];
    categories.forEach(cat => {
      cat.auditRefs.forEach( ar => {
        if (ar.weight > 0)
        refs.push({
          request_id:request_id,
          category:cat.id,
          audit_id:ar.id,
          weight: ar.weight,
          group: ar.group
        });
      });
    });

    await insertData(refs,'performance_poc','category_breakdown',local,bq);
}

export async function saveAudits(report: any, request_id: string, local: boolean, bq: boolean){
    const audits: Audit[] = Object.values(report.audits).map(element => {
        return element as Audit;
    });

    let refs: any[] = [];
    audits.forEach(ar => {
        if (ar.details && ar.details !== undefined) {
            if (ar.details.type) ar.type = ar.details.type;
            if (ar.details.overallSavingsMs) ar.overallSavingsMs = ar.details.overallSavingsMs
        }
        refs.push({
            request_id: request_id,
            id: ar.id,
            title: ar.title,
            description: ar.description,
            score: round(ar.score,4),
            score_display_mode: ar.scoreDisplayMode,
            numeric_value: round(ar.numericValue,4),
            numeric_unit: ar.numericUnit,
            display_value: ar.displayValue,
            type: ar.type,
            overall_saving_ms: round(ar.overallSavingsMs)
      });
    });

    await insertData(refs,'performance_poc','audits',local,bq);

}

export async function saveTiming(report: any, request_id: string,fetchTime: string, local: boolean, bq: boolean){

    let timing: any[] = [];
    report.timing.entries.forEach((el: Timing) => {
        timing.push({
            request_id: request_id,
            start_time: el.startTime,
            start_time_str: addMs(report.fetchTime, el.startTime),
            duration: el.duration,
            name: el.name,
            entry_type: el.entryType
        });
    });

    await insertData(timing,'performance_poc','timing',local,bq);
}

export async function saveGeneral(report: any, request_id: string, local: boolean, bq: boolean){
    const general = [
        {
            request_id: request_id,
            user_agent: report.userAgent,
            network_user_agent: report.environment.networkUserAgent,
            host_user_agent: report.environment.hostUserAgent,
            benchmark_index: report.environment.benchmarkIndex,
            lighthouse_version: report.lighthouseVersion,
            fetch_time: report.fetchTime,
            request_url: report.requestedUrl,
            final_url: report.finalUrl
        }
      ];
  
      await insertData(general,'performance_poc','general',local,bq);
}