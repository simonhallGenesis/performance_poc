import {AuditRef} from './audit-ref';

export interface Category {
    id: string;
    title: string;
    description: string;
    score: number;
    auditRefs: AuditRef[];
}