export interface Timing {
    startTime: number;
    duration: number;
    name: string;
    entryType: string;
}