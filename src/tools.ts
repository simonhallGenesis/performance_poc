import * as puppeteer from 'puppeteer';
import * as URL from 'url';
import {BigQuery} from '@google-cloud/bigquery';

export async function getLighthouseReport(url: string): Promise<any> {
    const browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox'],
    });

    console.log('Puppeteer launched\n');
    const lighthouse = require('lighthouse');

    // Lighthouse will open the URL.
    const {lhr} = await lighthouse(url, {
      port: URL.parse(browser.wsEndpoint()).port,
      output: 'json',
      logLevel: 'silent',
    });

    await browser.close();

    return lhr;
  }

  export function round(num: number, places?: number): number | undefined { 
    if (num && num != undefined && num!=null){
        let DIVISOR: number = 100;
        if (places && places >= 0 && places <=5){
            DIVISOR = Math.max(10 * places,1);
        }
        return Math.round((num + Number.EPSILON) * DIVISOR) / DIVISOR
    } else {
        return undefined;
    }
    
  }

  export function addMs(base: string, toAdd: number): string {
    const fetchTime: Date = new Date(base);
    let new_time: Date = new Date(fetchTime.valueOf() + toAdd);
    return new_time.toISOString();
  }

  export async function insertData(toAdd: any[],dataset: string,table: string, keyfile:boolean,bq:boolean ) {
    
    console.log("Inserting into " + table);

    if (!bq) {
      console.log("Inserting into " + table);
      if (toAdd.length === 0) {
        console.log("No data");
      } else {
        console.log(toAdd[0]);
      }
    }
    else {
      let opts = {};

      if (keyfile) {
        opts = {
          keyFilename: '../lh-server/key.file.json',
          projectId: 'sandbox-237614',
        }
      };

      // Create a client
      const bigqueryClient = new BigQuery(opts);
      await bigqueryClient
          .dataset(dataset)
          .table(table)
          .insert(toAdd)
          .catch(err => {
            if (err && err.name === 'PartialFailureError') {
                if (err.errors && err.errors.length > 0) {
                    console.log('Insert errors:');
                    console.error(err.errors[0]);
                }
            } else {
                console.error('ERROR:', err);
            }
        });
    
    }
    
  }