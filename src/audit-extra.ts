export interface AuditExtra {
    auditId: string;
    column: string;
    row: number;
    valueStr?: string;
    valueNum?: number;
}