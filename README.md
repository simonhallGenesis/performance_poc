# README #

This repository is used for performance testing websites. The typescript code can be transpiled into JavaScript and loaded into a GCP Cloud Function. It uses lighthouse (within puppeteer) and then writes the results to Big Query

### Local Development ###

The repository is tested locally and in the cloud with node v12 

The repository is written in typescript and contains 2 functions which can be uploaded to GCP. One is triggered by http requests and the other by messages froma. PubSub topic.

The web version is used for easier local testing and the event function is used in the cloud.

To start the code locally (accepting http requests) `npm run-script watch-web`

### Big Query Connection ###

A keyfile for a google service account is necessary to upload the results to BigQuery. The keyfile is not in version control and is referenced in tools.ts

Running the code without uploading the results to BigQuery can be done by setting the bq variable to false in the second parameter of lh_runner in index.ts


### Deploy to GCP ###

To deploy to GCP `npm run-script deploy`

Many values are hardcoded in package.json, including the function name, region and service account. In order for this script to succeed, you must be logged into gcloud locally with a service account with permission to upload cloud functions

This repository is only for poc purposes and contains no tests, code written by a Project Manager and no way to deploy unless you are logged into gcloud!